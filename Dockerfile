FROM ubuntu:18.04

MAINTAINER ASMik <cpp.create@gmail.com>

# For builds to succeed
ENV DEBIAN_FRONTEND=noninteractive

# Add user
RUN adduser --quiet --disabled-password qtuser

# Install Python 3, PyQt5
RUN apt-get update \
    && apt-get install -y \
          python3 \
          python3-pip \
          python3-pyqt5 \
          xvfb \
          python3-matplotlib \
          python3-pytest \
          python3-pytestqt 
RUN pip3 install qimage2ndarray
